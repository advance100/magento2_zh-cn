<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Composer\\Spdx\\' => array($vendorDir . '/composer/spdx-licenses/src'),
);
